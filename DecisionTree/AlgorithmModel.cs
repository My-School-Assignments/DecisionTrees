﻿/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Developed as part of a Machine Learning School assignment
 * 
 * Author: Dominik Hornak
 * All rights reserved (2017)
 * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Windows;
using DecisionTree.Common;

namespace DecisionTree
{
    public abstract class AlgorithmModel
    {
        public Node Tree { get; }
        protected String errors = String.Empty;

        public AlgorithmModel(DataSet data)
        {
            Init(data);
            if (errors == String.Empty)
            {
                Tree = GenerateTree(data);
            }
            else
            {
                MessageBox.Show(
                    "ID3 Does Cannot handle Continuous attributes in dataset! Please choose C45 Algorithm and try again or use dataset with discrete attributes only!");
            }
        }

        protected abstract void Init(DataSet data);

        public String[] Classify(DataSet data)
        {
            var result = new List<String>();
            foreach (var row in data)
            {
                result.Add(Classify(row, Tree));
            }

            return result.ToArray();
        }

        private String Classify(Example e, Node n)
        {
            if (n is ListNode)
            {
                return n.attr.Name;
            }

            if (n.attr.Type == AttributeType.Discrete)
            {
                return Classify(e, ((DecisionNode) n).Children.Find(c =>
                {
                    var key = e.Row.Keys.Where(r => r.Name.Equals(n.attr.Name));
                    return c.EdgeValue.Equals(e.Row[key.First()]);
                }));
            }

            return Classify(e, ((DecisionNode)n).Children.Find(c =>
            {
                if (c.EdgeValue.Contains(">"))
                {
                    var val = Double.Parse(c.EdgeValue.Substring(2));
                    var key = e.Row.Keys.Where(r => r.Name.Equals(n.attr.Name));
                    return Double.Parse(e.Row[key.First()]) >= val;
                }
                else if (c.EdgeValue.Contains("<"))
                {
                    var val = Double.Parse(c.EdgeValue.Substring(1));
                    var key = e.Row.Keys.Where(r => r.Name.Equals(n.attr.Name));
                    return Double.Parse(e.Row[key.First()]) < val;
                }
                else
                {
                    return false;
                }
            }));
        }

        private Node GenerateTree(DataSet instances, String edge = null)
        {
            var label = checkClassDistribution(instances);
            if (label != null)
            {
                return new ListNode(label, edge);
            }

            if (instances.DataAttributes.Count == 0)
            {
                var mostCommon = getMostCommon(instances);
                return new ListNode(mostCommon, edge);
            }

            var best = getBest(instances);
            Node root = new DecisionNode(best, edge);

            if (best.Type == AttributeType.Discrete && best.ValuesList != null)
            {
                foreach (var v in best.ValuesList)
                {
                    var subSet = getSubSet(instances, best, v);
                    if (subSet.NumberOfAttributes == 0)
                    {
                        var mCom = getMostCommon(instances);
                        //((DecisionNode)root).AddChild(new ListNode(mCom, edge));
                    }

                    ((DecisionNode) root).AddChild(GenerateTree(subSet, v));
                }
            }
            else
            {
                var uSet = getSubSet(instances, best, 'u');
                var dSet = getSubSet(instances, best, 'd');

                if (uSet.NumberOfAttributes == 0)
                {
                    var mUCom = getMostCommon(instances);
                    //((DecisionNode)root).AddChild(new ListNode(mUCom, edge));
                }

                if (dSet.NumberOfAttributes == 0)
                {
                    var mDCom = getMostCommon(instances);
                    //((DecisionNode)root).AddChild(new ListNode(mDCom, edge));
                }

                ((DecisionNode)root).AddChild(GenerateTree(uSet, ">= " + best.BestBorder));
                ((DecisionNode)root).AddChild(GenerateTree(dSet, "< " + best.BestBorder));
            }

            return root;
        }

        protected abstract Attribute getBest(DataSet instances);

        private DataSet getSubSet(DataSet dat, Attribute withoutThis, Char flag)
        {
            var result = new DataSet();

            if (flag == 'u')
            {
                result.TargetAttribute = dat.TargetAttribute;

                foreach (var attr in dat.DataAttributes)
                {
                    if (attr != withoutThis)
                    {
                        result.AddAtribute(attr);
                    }
                }

                foreach (var ex in dat)
                {
                    if (Double.Parse(ex.Row[withoutThis]) >= withoutThis.BestBorder)
                    {
                        var newEx = new Example();
                        foreach (var col in ex.Row)
                        {
                            if (col.Key != withoutThis)
                            {
                                newEx.AddColumn(col.Key, col.Value);
                            }
                        }

                        result.AddExample(newEx);
                    }
                }
            }
            else if(flag == 'd')
            {
                result.TargetAttribute = dat.TargetAttribute;

                foreach (var attr in dat.DataAttributes)
                {
                    if (attr != withoutThis)
                    {
                        result.AddAtribute(attr);
                    }
                }

                foreach (var ex in dat)
                {
                    if (Double.Parse(ex.Row[withoutThis]) < withoutThis.BestBorder)
                    {
                        var newEx = new Example();
                        foreach (var col in ex.Row)
                        {
                            if (col.Key != withoutThis)
                            {
                                newEx.AddColumn(col.Key, col.Value);
                            }
                        }

                        result.AddExample(newEx);
                    }
                }
            }

            return result;
        }

        private DataSet getSubSet(DataSet dat, Attribute withoutThis, String val)
        {
            var result = new DataSet();

            result.TargetAttribute = dat.TargetAttribute;

            foreach (var attr in dat.DataAttributes)
            {
                if (attr != withoutThis)
                {
                    result.AddAtribute(attr);
                }
            }

            foreach (var ex in dat)
            {
                if (ex.Row[withoutThis].Equals(val))
                {
                    var newEx = new Example();
                    foreach (var col in ex.Row)
                    {
                        if (col.Key != withoutThis)
                        {
                            newEx.AddColumn(col.Key, col.Value);
                        }
                    }

                    result.AddExample(newEx);
                }
            }

            return result;
        }

        private Attribute getMostCommon(DataSet dat)
        {
            Dictionary<String, int> val = new Dictionary<String, int>();
            foreach (var attr in dat.TargetAttribute.ValuesList)
            {
                val.Add(attr, 0);
            }

            foreach (var row in dat)
            {
                if (val.ContainsKey(row.Row[dat.TargetAttribute]))
                {
                    val[row.Row[dat.TargetAttribute]]++;
                }
            }

            String result = null;
            int resNum = 0;
            foreach (var v in val)
            {
                if (v.Value > resNum)
                {
                    resNum = v.Value;
                    result = v.Key;
                }
            }

            return new Attribute(result, AttributeType.Discrete);
        }

        private Attribute checkClassDistribution(DataSet dat)
        {
            String val = null;
            foreach (var row in dat)
            {
                if (val == null)
                {
                    val = row.Row[dat.TargetAttribute];
                    continue;
                }

                if (val != row.Row[dat.TargetAttribute])
                {
                    return null;
                }
            }

            return new Attribute(val, AttributeType.Discrete);
        }
    }
}