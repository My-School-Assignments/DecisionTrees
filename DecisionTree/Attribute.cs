﻿/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Developed as part of a Machine Learning School assignment
 * 
 * Author: Dominik Hornak
 * All rights reserved (2017)
 * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

using System;
using System.Collections.Generic;
using System.Linq;
using DecisionTree.Common;

namespace DecisionTree
{
    public class Attribute
    {
        public AttributeType Type { get; }
        public String Name { get; }
        public HashSet<String> ValuesList { get; }

        public SortedSet<double> UniqueValues { get; }
        public SortedSet<double> PossibleBorders { get; }
        public double BestBorder { get; set; }

        public Attribute(String name, AttributeType type)
        {
            this.Name = name;
            this.Type = type;
            if (type == AttributeType.Discrete)
            {
                this.ValuesList = new HashSet<string>();
            }
            else
            {
                UniqueValues = new SortedSet<double>();
                PossibleBorders = new SortedSet<double>();
            }
        }

        public void AddDiscreteValue(String val)
        {
            if (Type == AttributeType.Discrete)
            {
                ValuesList.Add(val);
            }
        }

        public void AddContinuousValue(double val)
        {
            if (Type == AttributeType.Continuous)
            {
                UniqueValues.Add(val);
            }
        }

        public void CalculateContinuousBorders()
        {
            if (Type == AttributeType.Continuous)
            {
                var vals = UniqueValues.ToList();
                for (var i = 1; i < vals.Count; ++i)
                {
                    var avg = vals[i - 1] + vals[i];
                    PossibleBorders.Add(avg / 2.0d);
                }
            }
        }
    }
}
