﻿/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Developed as part of a Machine Learning School assignment
 * 
 * Author: Dominik Hornak
 * All rights reserved (2017)
 * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

using System;
using DecisionTree.Common;

namespace DecisionTree
{
    internal class C45 : AlgorithmModel
    {
        public C45(DataSet data)
            : base(data)
        {}

        protected override void Init(DataSet data)
        {
            foreach (var attr in data.DataAttributes)
            {
                if (attr.Type == AttributeType.Continuous)
                {
                    attr.CalculateContinuousBorders();
                }
            }
        }

        protected override Attribute getBest(DataSet instances)
        {
            if (instances.NumberOfAttributes == 1)
            {
                return instances.DataAttributes[0];
            }

            double datasetEntropy = calculateEntropy(instances, instances.TargetAttribute);

            Attribute best = null;
            double informationGain = 0;

            foreach (var attr in instances.DataAttributes)
            {
                if (attr.Type == AttributeType.Discrete)
                {
                    double ent = calculateEntropy(instances, attr);
                    double infGain = datasetEntropy - ent;
                    double metricEntropy = calculateMetricEntropy(instances, attr);
                    double metricGain = infGain / metricEntropy;

                    if (metricGain > informationGain)
                    {
                        informationGain = metricGain;
                        best = attr;
                    }
                }
                else
                {
                    double bestMetricGain = 0;
                    double bestBorder = 0;

                    foreach (var border in attr.PossibleBorders)
                    {
                        double probLo = calculateTargetLowerProbability(instances, attr, border);
                        double probHi = calculateTargetHigherProbability(instances, attr, border);
                        double ent =
                             probLo * calculateLowerEntropy(instances, attr, border) +
                             probHi * calculateHigherEntropy(instances, attr, border);
                        double infGain = datasetEntropy - ent;
                        double metricEntropy = 0;
                        if (probLo != 0)
                        {
                            metricEntropy -= probLo * Math.Log(probLo, 2);
                        }
                        if (probHi != 0)
                        {
                            metricEntropy -= probHi * Math.Log(probHi, 2);
                        }

                        double metricGain = infGain / metricEntropy;

                        if (metricGain > bestMetricGain)
                        {
                            bestMetricGain = metricGain;
                            bestBorder = border;
                        }
                    }

                    attr.BestBorder = bestBorder;
                    if (bestMetricGain > informationGain)
                    {
                        informationGain = bestMetricGain;
                        best = attr;
                    }
                }
            }

            return best;
        }

        private double calculateHigherEntropy(DataSet data, Attribute attr, double border)
        {
            double result = 0;

            foreach (var value in data.TargetAttribute.ValuesList)
            {
                double prob = calculateValueHigherProbability(data, attr, border, value);
                if (prob != 0)
                {
                    result -= prob * Math.Log(prob, 2);
                }
            }

            return result;
        }

        private double calculateValueHigherProbability(DataSet data, Attribute attr, double border, String targetVal)
        {
            ulong numPositive = 0;
            ulong numTotal = 0;

            foreach (var row in data)
            {
                if (Double.Parse(row.Row[attr]) >= border)
                {
                    numTotal++;

                    if (row.Row[data.TargetAttribute] == targetVal)
                    {
                        numPositive++;
                    }
                }
            }

            if (numTotal == 0)
            {
                return 0;
            }

            return numPositive / (double)numTotal;
        }

        private double calculateLowerEntropy(DataSet data, Attribute attr, double border)
        {
            double result = 0;

            foreach (var value in data.TargetAttribute.ValuesList)
            {
                double prob = calculateValueLowerProbability(data, attr, border, value);
                if (prob != 0)
                {
                    result -= prob * Math.Log(prob, 2);
                }
            }

            return result;
        }

        private double calculateValueLowerProbability(DataSet data, Attribute attr, double border, String targetVal)
        {
            ulong numPositive = 0;
            ulong numTotal = 0;

            foreach (var row in data)
            {
                if (Double.Parse(row.Row[attr]) < border)
                {
                    numTotal++;

                    if (row.Row[data.TargetAttribute] == targetVal)
                    {
                        numPositive++;
                    }
                }
            }

            if (numTotal == 0)
            {
                return 0;
            }

            return numPositive / (double)numTotal;
        }

        private double calculateTargetHigherProbability(DataSet data, Attribute attr, double border)
        {
            ulong numPositive = 0;
            ulong numTotal = data.NumberOfExamples;

            foreach (var row in data)
            {
                if (Double.Parse(row.Row[attr]) >= border)
                {
                    numPositive++;
                }
            }

            if (numTotal == 0)
            {
                return 0;
            }

            return numPositive / (double)numTotal;
        }

        private double calculateTargetLowerProbability(DataSet data, Attribute attr, double border)
        {
            ulong numPositive = 0;
            ulong numTotal = data.NumberOfExamples;

            foreach (var row in data)
            {
                if (Double.Parse(row.Row[attr]) < border)
                {
                    numPositive++;
                }
            }

            if (numTotal == 0)
            {
                return 0;
            }

            return numPositive / (double)numTotal;
        }

        private double calculateMetricEntropy(DataSet data, Attribute attr)
        {
            double result = 0;
            if (attr.Type == AttributeType.Discrete)
            {
                foreach (var value in attr.ValuesList)
                {
                    double prob = calculateTargetProbability(data, attr, value);
                    if (prob != 0)
                    {
                        result -= prob * Math.Log(prob, 2);
                    }
                }

                return result;
            }

            return -1;
        }

        private double calculateEntropy(DataSet data, Attribute entAttr)
        {
            double result = 0;
            if (entAttr.Type == AttributeType.Discrete)
            {
                if (entAttr == data.TargetAttribute)
                {
                    foreach (var value in entAttr.ValuesList)
                    {
                        double prob = calculateTargetProbability(data, entAttr, value);
                        if (prob != 0)
                        {
                            result -= prob * Math.Log(prob, 2);
                        }
                    }

                    return result;
                }

                foreach (var value in entAttr.ValuesList)
                {
                    double partialProbability = calculateTargetProbability(data, entAttr, value);
                    double partialEntropy = 0;

                    foreach (var targVal in data.TargetAttribute.ValuesList)
                    {
                        double prob = calculateValueProbability(data, entAttr, value, targVal);
                        if (prob != 0)
                        {
                            partialEntropy -= prob * Math.Log(prob, 2);
                        }
                    }

                    result += partialProbability * partialEntropy;
                }

                return result;
            }

            return -1;
        }

        private double calculateValueProbability(DataSet data, Attribute attr, String attrVal, String targetVal)
        {
            ulong numPositive = 0;
            ulong numTotal = 0;

            foreach (var row in data)
            {
                if (row.Row[attr].Equals(attrVal))
                {
                    numTotal++;

                    if (row.Row[data.TargetAttribute] == targetVal)
                    {
                        numPositive++;
                    }
                }
            }

            if (numTotal == 0)
            {
                return 0;
            }

            return numPositive / (double)numTotal;
        }

        private double calculateTargetProbability(DataSet data, Attribute entAttr, String value)
        {
            ulong numPositive = 0;
            ulong numTotal = data.NumberOfExamples;

            foreach (var row in data)
            {
                if (row.Row[entAttr].Equals(value))
                {
                    numPositive++;
                }
            }

            if (numTotal == 0)
            {
                return 0;
            }

            return numPositive / (double)numTotal;
        }
    }
}
