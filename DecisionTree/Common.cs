﻿/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Developed as part of a Machine Learning School assignment
 * 
 * Author: Dominik Hornak
 * All rights reserved (2017)
 * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

namespace DecisionTree.Common
{
    public enum LearningAlg
    {
        ID3 = 0,
        C45 = 1
    }

    public enum AttributeValidationType
    {
        Entropy = 0,
        InformationGain = 1
    }

    public enum AttributeType
    {
        Discrete = 0,
        Continuous = 1
    }
}
