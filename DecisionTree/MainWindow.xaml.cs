﻿/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Developed as part of a Machine Learning School assignment
 * 
 * Author: Dominik Hornak
 * All rights reserved (2017)
 * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

using System;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using DecisionTree.Common;
using Microsoft.Msagl.Drawing;
using Microsoft.Win32;

namespace DecisionTree
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private DataSet data;
        private DecTree tree;
        private Graph graph;
        private int count = 0;
        private LearningAlg alg = LearningAlg.ID3;
        private bool GroupSame = true;

        public MainWindow()
        {
            InitializeComponent();

            GenerateGraph();
        }

        private void GenerateGraph()
        {
            graph = new Graph("graph");
            this.gViewer.BackColor = System.Drawing.Color.FromArgb(25, 25, 25);
            this.gViewer.ToolBarIsVisible = false;
            this.gViewer.PanButtonPressed = true;
        }

        private void btnOpenFile_Click(Object sender, RoutedEventArgs e)
        {
            data = null;
            tree = null;

            OpenFileDialog openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == true)
            {
                try
                {
                    data = new DataSet(openFileDialog.FileName);
                }
                catch (InvalidDataException exception)
                {
                    MessageBox.Show(exception.Message + "\n Please provide valid input file.");
                    return;
                }
                tree = new DecTree(data, alg);
                if (tree.Model.Tree == null)
                {
                    btnPredictFromModel.IsEnabled = false;
                    tree = null;
                }
                else
                {
                    btnPredictFromModel.IsEnabled = true;
                    Draw();
                }
            }
        }

        private void Draw()
        {
            graph = new Graph("Graph");
            count = 0;
            this.gViewer.Graph = null;

            drawTree(tree.Model.Tree);
            graph.Attr.BackgroundColor = new Microsoft.Msagl.Drawing.Color(25, 25, 25);
            graph.Attr.Color = new Microsoft.Msagl.Drawing.Color(25, 25, 25);

            foreach (var node in graph.Nodes)
            {
                node.Attr.FillColor = new Microsoft.Msagl.Drawing.Color(50, 50, 50);
                node.Attr.LineWidth = 1;
                node.Attr.Color = new Microsoft.Msagl.Drawing.Color(195, 195, 195);
                node.Label.FontColor = new Microsoft.Msagl.Drawing.Color(195, 195, 195);

                foreach (var edge in node.Edges)
                {
                    edge.Attr.Color = new Microsoft.Msagl.Drawing.Color(195, 195, 195);
                    edge.Label.FontColor = new Microsoft.Msagl.Drawing.Color(195, 195, 195);
                }
            }

            this.gViewer.Graph = graph;
        }

        private void drawTree(Node node, String prevNode = null, int prevNodeCount = -1, int nodeCount = 0)
        {
            int nC = nodeCount;
            int pC = prevNodeCount;
            if (prevNode != null)
            {
                if (node is ListNode)
                {
                    count++;
                    if (GroupSame)
                    {
                        graph.AddEdge(prevNode + pC, node.EdgeValue,
                            node.attr.Name == null ? "NULLL" : node.attr.Name);
                    }
                    else
                    {
                        graph.AddEdge(prevNode + pC, node.EdgeValue,
                            node.attr.Name == null ? "NULLL" + count : node.attr.Name + count);
                    }
                }
                else
                {
                    graph.AddEdge(prevNode + pC, node.EdgeValue, node.attr.Name == null ? "NULLL" : node.attr.Name + nC);
                }
            }

            if (node is DecisionNode)
            {
                foreach (var kid in ((DecisionNode)node).Children)
                {
                    drawTree(kid, node.attr.Name, nC, ++nodeCount);
                }
            }
        }

        private void MethodChange(Object sender, RoutedEventArgs e)
        {
            if ((RadioButton)(sender) == this.Id3Checked)
            {
                if (C45Checked != null)
                {
                    btnPredictFromModel.IsEnabled = false;
                    this.C45Checked.IsChecked = false;
                }
                alg = LearningAlg.ID3;

                if (this.gViewer != null)
                {
                    this.gViewer.Graph = null;
                }
            }
            else if ((RadioButton)(sender) == this.C45Checked)
            {
                if (Id3Checked != null)
                {
                    btnPredictFromModel.IsEnabled = false;
                    this.Id3Checked.IsChecked = false;
                }
                alg = LearningAlg.C45;

                if (this.gViewer != null)
                {
                    this.gViewer.Graph = null;
                }
            }
        }

        private void btnPredictFromModel_Click(Object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == true)
            {
                data = new DataSet(openFileDialog.FileName, true);
                var results = tree.Model.Classify(data);
                string result = "";
                int i = 0;

                using (var reader = new StreamReader(openFileDialog.FileName))
                {
                    while (!reader.EndOfStream)
                    {
                        result += reader.ReadLine();
                        if (i == 0)
                        {
                            result += ", Class\n";
                        }
                        else
                        {
                            result += ", " + results[i - 1] + "\n";
                        }
                        i++;
                    }
                }

                var newFile = openFileDialog.FileName.Replace(".csv", "_results.csv");
                File.WriteAllText(newFile, result);
            }
        }

        private void StyleChange(Object sender, RoutedEventArgs e)
        {
            if ((RadioButton)(sender) == this.Group)
            {
                if (Distinct != null)
                {
                    Distinct.IsChecked = false;
                }
                GroupSame = true;

                if (tree != null)
                {
                    Draw();
                }
            }
            else if ((RadioButton)(sender) == this.Distinct)
            {
                if (Group != null)
                {
                    this.Group.IsChecked = false;
                }
                GroupSame = false;

                if (tree != null)
                {
                    Draw();
                }
            }
        }
    }
}
