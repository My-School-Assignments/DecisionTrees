﻿/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Developed as part of a Machine Learning School assignment
 * 
 * Author: Dominik Hornak
 * All rights reserved (2017)
 * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

using System;
using System.Collections.Generic;

namespace DecisionTree
{
    public abstract class Node
    {
        public Attribute attr { get; protected set; }
        public String EdgeValue { get; protected set; }

        protected Node(Attribute attr, String edge)
        {
            this.attr = attr;
            this.EdgeValue = edge;
        }
    }

    public class ListNode : Node
    {
        public ListNode(Attribute attr, String edge) 
            :base(attr, edge)
        { }
    }

    public class DecisionNode : Node
    {
        public List<Node> Children { get; }

        public DecisionNode(Attribute attr, String edge) 
            :base(attr, edge)
        {
            Children = new List<Node>();
        }

        public void AddChild(Node kid)
        {
            Children.Add(kid);
        }
    }
}
