﻿/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Developed as part of a Machine Learning School assignment
 * 
 * Author: Dominik Hornak
 * All rights reserved (2017)
 * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

using System;
using System.Windows;
using DecisionTree.Common;

namespace DecisionTree
{
    public class DecTree
    {
        private DataSet data;
        private LearningAlg alg;
        private AttributeValidationType avt;

        public AlgorithmModel Model { get; private set; }

        public DecTree(DataSet data, LearningAlg alg = LearningAlg.ID3, AttributeValidationType avt = AttributeValidationType.InformationGain)
        {
            this.data = data;
            this.alg = alg;
            this.avt = avt;

            init();
        }

        public String[] Classify(DataSet data)
        {
            return Model.Classify(data);
        }

        private void init()
        {
            if (alg == LearningAlg.ID3)
            {
                Model = new ID3(data);
                return;
            }
            else if (alg == LearningAlg.C45)
            {
                if (avt == AttributeValidationType.Entropy)
                {
                    MessageBox.Show(
                        "C45 does not support Entropy as Attribute validation parameter! Using Information Gain instad!");
                }
                Model = new C45(data);
                return;
            }

            throw new NotSupportedException("Only ID3 and C4.5 algorithms are currently supported!");
        }
    }
}
