﻿/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Developed as part of a Machine Learning School assignment
 * 
 * Author: Dominik Hornak
 * All rights reserved (2017)
 * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

using System;
using DecisionTree.Common;

namespace DecisionTree
{
    internal class ID3 : AlgorithmModel
    {
        private AttributeValidationType avt = AttributeValidationType.InformationGain;

        public ID3(DataSet data)
            : base(data)
        {
        }

        protected override void Init(DataSet data)
        {
            bool anyContinuous = false;
            foreach (var att in data.DataAttributes)
            {
                if (att.Type == AttributeType.Continuous)
                {
                    anyContinuous = true;
                    break;
                }
            }

            if (anyContinuous)
            {
                errors =
                    "ID3 Does Cannot handle Continuous attributes in dataset! Please choose C45 Algorithm and try again or use dataset with discrete attributes only!";
            }
        }

        protected override Attribute getBest(DataSet instances)
        {
            if (avt == AttributeValidationType.InformationGain)
            {
                double datasetEntropy = calculateEntropy(instances, instances.TargetAttribute);

                Attribute best = null;
                double informationGain = 0;

                foreach (var attr in instances.DataAttributes)
                {
                    double ent = calculateEntropy(instances, attr);
                    double infGain = datasetEntropy - ent;
                    double metricEntropy = calculateMetricEntropy(instances, attr);
                    double metricGain = infGain / metricEntropy;

                    if (metricGain > informationGain)
                    {
                        informationGain = metricGain;
                        best = attr;
                    }
                }

                return best;
            }
            else
            {
                Attribute best = null;
                double entropy = Double.MaxValue;

                foreach (var attr in instances.DataAttributes)
                {
                    double ent = calculateEntropy(instances, attr);
                    if (ent < entropy)
                    {
                        entropy = ent;
                        best = attr;
                    }
                }

                return best;
            }
        }

        private double calculateMetricEntropy(DataSet data, Attribute attr)
        {
            double result = 0;
            if (attr.Type == AttributeType.Discrete)
            {
                foreach (var value in attr.ValuesList)
                {
                    double prob = calculateTargetProbability(data, attr, value);

                    if (prob != 0)
                    {
                        result -= prob * Math.Log(prob, 2);
                    }
                }

                return result;
            }

            return -1;
        }

        private double calculateEntropy(DataSet data, Attribute entAttr)
        {
            double result = 0;
            if (entAttr.Type == AttributeType.Discrete)
            {
                if (entAttr == data.TargetAttribute)
                {
                    foreach (var value in entAttr.ValuesList)
                    {
                        double prob = calculateTargetProbability(data, entAttr, value);
                        if (prob != 0)
                        {
                            result -= prob * Math.Log(prob, 2);
                        }
                    }

                    return result;
                }

                foreach (var value in entAttr.ValuesList)
                {
                    double partialProbability = calculateTargetProbability(data, entAttr, value);
                    double partialEntropy = 0;

                    foreach (var targVal in data.TargetAttribute.ValuesList)
                    {
                        double prob = calculateValueProbability(data, entAttr, value, targVal);
                        if (prob != 0)
                        {
                            partialEntropy -= prob * Math.Log(prob, 2);
                        }
                    }

                    result += partialProbability * partialEntropy;
                }

                return result;
            }

            return -1;
        }

        private double calculateValueProbability(DataSet data, Attribute attr, String attrVal, String targetVal)
        {
            ulong numPositive = 0;
            ulong numTotal = 0;

            foreach (var row in data)
            {
                if (row.Row[attr].Equals(attrVal))
                {
                    numTotal++;

                    if (row.Row[data.TargetAttribute] == targetVal)
                    {
                        numPositive++;
                    }
                }
            }

            if (numTotal == 0)
            {
                return 0;
            }

            return numPositive / (double)numTotal;

        }

        private double calculateTargetProbability(DataSet data, Attribute entAttr, String value)
        {
            ulong numPositive = 0;
            ulong numTotal = data.NumberOfExamples;

            foreach (var row in data)
            {
                if (row.Row[entAttr].Equals(value))
                {
                    numPositive++;
                }
            }

            if (numTotal == 0)
            {
                return 0;
            }

            return numPositive / (double) numTotal;
        }
    }
}
