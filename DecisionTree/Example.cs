﻿/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Developed as part of a Machine Learning School assignment
 * 
 * Author: Dominik Hornak
 * All rights reserved (2017)
 * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

using System;
using System.Collections.Generic;

namespace DecisionTree
{
    public class Example
    {
        public Dictionary<Attribute, String> Row { get; }

        public Example()
        {
            Row = new Dictionary<Attribute, string>();
        }

        public void AddColumn(Attribute attr, String val)
        {
            Row.Add(attr, val);
        }
    }
}
