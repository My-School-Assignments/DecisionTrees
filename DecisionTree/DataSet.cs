﻿/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Developed as part of a Machine Learning School assignment
 * 
 * Author: Dominik Hornak
 * All rights reserved (2017)
 * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using DecisionTree.Common;

namespace DecisionTree
{
    public class DataSet : IEnumerable<Example>
    {
        public ulong NumberOfAttributes { get; private set; }
        public ulong NumberOfExamples { get; private set; }

        public Attribute TargetAttribute
        {
            get { return _targetAttr; }
            set
            {
                this._targetAttr = value;
                initializeData();
            }
        }

        public bool IsInitialized { get; private set; }
        private List<Example> dataRows;
        public List<Attribute> DataAttributes { get; }
        private Attribute _targetAttr;

        public DataSet(String path, bool classification = false)
            : this()
        {
            if (Path.GetExtension(path) == ".csv")
            {
                loadCSV(path);
            }

            if (!IsInitialized && !classification)
            {
                throw new InvalidDataException("Data in file specified are invalid! Path: " + path);
            }
        }

        public DataSet()
        {
            dataRows = new List<Example>();
            DataAttributes = new List<Attribute>();
            NumberOfAttributes = 0;
            NumberOfExamples = 0;
            TargetAttribute = null;
            IsInitialized = false;
        }

        public void AddAtribute(Attribute atr)
        {
            DataAttributes.Add(atr);
            NumberOfAttributes++;
            initializeData();
        }

        public void AddExample(Example ex)
        {
            if ((ulong)ex.Row.Count == NumberOfAttributes + 1)
            {
                dataRows.Add(ex);
                NumberOfExamples++;
                initializeData();
            }
        }

        private void loadCSV(String path)
        {
            using (var reader = new StreamReader(path))
            {
                if (reader.EndOfStream)
                {
                    return;
                }
                var firstLine = true;
                var secondLine = false;
                List<String> attrNames = new List<string>();
                List<Attribute> attrs = new List<Attribute>();
                List<List<String>> data = new List<List<String>>();

                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    var values = line.Split(',');
                    for (var i = 0; i < values.Length; ++i)
                    {
                        values[i] = values[i].Replace(" ", String.Empty);
                    }

                    if (firstLine)
                    {
                        foreach (var v in values)
                        {
                            attrNames.Add(v);
                        }

                        firstLine = false;
                        secondLine = true;
                    }
                    else if (secondLine)
                    {
                        for (var i = 0; i < values.Length; ++i)
                        {
                            var col = new List<String>();
                            col.Add(values[i]);
                            Double tmp;
                            Attribute attr;
                            if (Double.TryParse(values[i], NumberStyles.Number, new NumberFormatInfo(), out tmp) && i < (values.Length - 1))
                            {
                                attr = new Attribute(attrNames[i], AttributeType.Continuous);
                                attr.AddContinuousValue(tmp);
                                attrs.Add(attr);
                            }
                            else
                            {
                                attr = new Attribute(attrNames[i], AttributeType.Discrete);
                                attr.AddDiscreteValue(values[i]);
                                attrs.Add(attr);
                            }

                            data.Add(col);
                        }

                        attrNames.Clear();
                        secondLine = false;
                    }
                    else
                    {
                        for (var i = 0; i < values.Length; ++i)
                        {
                            data[i].Add(values[i]);
                            Double tmp;
                            Attribute attr = attrs[i];
                            if (Double.TryParse(values[i], NumberStyles.Number, new NumberFormatInfo(), out tmp) && i < (values.Length - 1))
                            {
                                attr.AddContinuousValue(tmp);
                            }
                            else
                            {
                                attr.AddDiscreteValue(values[i]);
                            }
                        }
                    }
                }

                this.TargetAttribute = attrs[attrs.Count - 1];
                for (var i = 0; i < attrs.Count - 1; ++i)
                {
                    this.AddAtribute(attrs[i]);
                }

                for (var i = 0; i < data[0].Count; ++i)
                {
                    var ex = new Example();

                    for (var j = 0; j < attrs.Count; ++j)
                    {
                        ex.AddColumn(attrs[j], data[j][i]);
                    }

                    this.AddExample(ex);
                }
            }
        }

        private void initializeData()
        {
            if (NumberOfAttributes != 0 && NumberOfExamples != 0 && TargetAttribute != null && 
                dataRows != null && dataRows.Count > 3 &&
                DataAttributes != null && DataAttributes.Count > 0)
            {
                IsInitialized = true;
            }
        }

        public IEnumerator<Example> GetEnumerator()
        {
            return dataRows.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}